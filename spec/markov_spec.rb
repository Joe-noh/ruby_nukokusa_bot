#-*- coding: utf-8 -*-

require_relative 'spec_helper'

describe NukokusaBot::MarkovChain do

  before(:all) do
    @markov = NukokusaBot::MarkovChain.new
  end

  after(:all) do
    File.delete File.join(File.dirname(__FILE__), '..', 'db', 'test.db')
  end

  it '形態素解析ができる' do
    expect(@markov.send :parse_text, 'チクワです').to match_array ['チクワ', 'です']
  end

  it '解析結果をDBに格納できる' do
    ids = [12345, 23456]
    statuses = ['これはチクワです', 'チクワです']
    conn = @markov.instance_variable_get(:@db_conn)
    ids.zip(statuses).each do |id, status|
      @markov.add_sentence(id, status)
    end
    words = conn.execute('SELECT surface FROM words;').collect{|word| word['surface']}.flatten
    links = conn.execute('SELECT pre_id, suc_id, count FROM links;').map{|row| row.slice('pre_id', 'suc_id', 'count')}

    expect(words).to match_array ['これ', 'は', 'チクワ', 'です']
    expect(links).to match_array [
      {'pre_id' => 1, 'suc_id' => 2, 'count' => 1},
      {'pre_id' => 2, 'suc_id' => 3, 'count' => 1},
      {'pre_id' => 3, 'suc_id' => 4, 'count' => 2}
    ]
  end

  it '文章を生成できる' do
    sentence = @markov.generate_sentence
    expect(sentence.length).to be_between(1, 140)
  end
end
