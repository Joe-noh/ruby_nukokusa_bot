#-*- coding: utf-8 -*-

require_relative 'spec_helper'

describe Hash do

  it '指定したキーに該当するもの以外を削除して返せる' do
    hash = {a: 1, b: 2, c: 3}
    expect(hash.slice :a, :c).to eql({a: 1, c: 3})
  end
end