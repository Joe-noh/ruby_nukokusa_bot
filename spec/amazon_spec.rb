#-*- coding: utf-8 -*-

require_relative 'spec_helper'

describe NukokusaBot::AmazonAssociate do
  include_context 'load config file'

  before do
    @amazon = NukokusaBot::AmazonAssociate.new(@conf['amazon'])
  end

  it '商品をキーワード検索できる' do
    items = @amazon.search('ちくわ')

    items.each do |item|
      expect(item.keys).to match_array([:title, :price, :url])
    end
  end
end
