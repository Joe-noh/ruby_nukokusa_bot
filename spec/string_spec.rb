#-*- coding: utf-8 -*-

require_relative 'spec_helper'

describe String do

  it 'URLが含まれているか判定できる' do
    expect('URLはhttp://www.google.co.jpです'.contains_url?).to be_true
    expect('https://twitter.com/nukokusa_bot'.contains_url?).to be_true
    expect('http://examp.le?q=abc&oe=utf-8').to be_true
    expect('http通信'.contains_url?).to be_false
  end

  it 'メンションの宛名を取り除ける' do
    expect('@nukokusa おい'.delete_mention).to eql ' おい'
    expect('へい @nukokusa'.delete_mention).to eql 'へい '
    expect(     'チクワです'.delete_mention).to eql 'チクワです'
  end
end