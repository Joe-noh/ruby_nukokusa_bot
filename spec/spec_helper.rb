#-*- coding: utf-8 -*-

ENV['nukokusabot.env'] = 'test'

require_relative '../lib/nukokusa_bot'

shared_context 'load config file' do
  before :all do
    yml = File.join(File.dirname(__FILE__), '..', 'conf.yml')
    @conf = YAML.load_file yml
  end
end
