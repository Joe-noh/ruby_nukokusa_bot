#-*- coding: utf-8 -*-

require 'yaml'
require 'open-uri'
require 'kconv'
require 'date'

require 'bundler'
Bundler.require(:default, (ENV['nukokusabot.env'] || 'dev').to_sym)

pattern = File.join(File.expand_path('../nukokusa_bot', __FILE__), '*')

Dir.glob(pattern) do |fname|
  require fname
end
