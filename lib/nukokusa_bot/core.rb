#-*- coding: utf-8 -*-

module NukokusaBot

  def run(yml_file, option)
    Core.new(yml_file, option)
  rescue Exception => e
    puts e.inspect, e.backtrace
    sleep 15
    retry
  end
  module_function :run

  class Core
    def initialize(yml_file, option)
      @config = YAML.load_file(yml_file)

      twitter_auth

      @markov = MarkovChain.new
      @amazon = AmazonAssociate.new(@config['amazon'])
      @stream = TweetStream::Client.new
      @scheduler = Rufus::Scheduler.new

      if option[:crawl]
        puts "start crawling"
        crawl_twilog
        puts "crawling finished"
      end

      if option[:rebuild]
        puts "start morpheme analysing"
        @markov.rebuild_dictionary
        puts "analysing finished"
      end

      @scheduler.cron '30 * * * *' do
        #@twitter.update_status @markov.generate_string
        puts "[markov] " + @markov.generate_sentence
      end

      @stream.on_timeline_status do |status|
        text = status.text.delete_mention
        from_user = status.from_user
        if status.text.start_with? "@nukokusa_bot" || status.in_reply_to_screen_name == "nukokusa_bot"
          wanna_buy = text.scan(/(.+)が?(欲しい|ほしい|買いたい)/).flatten
          unless wanna_buy.empty?
            item = @amazon.search(wanna_buy.first).first
            Twitter.update_status("#{item.title} #{item.price} #{item.url}".append_timestamp)
          else
          end
        elsif from_user == 'JO_RI' || from_user == 'shogo82148'
          now = DateTime.now
          not_holiday  = !(now.sunday? || now.saturday? || now.national_holiday?)
          working_hour = (9..10).cover?(now.hour) || (14..15).cover?(now.hour)
          if rand(32) == 0 && not_holiday && working_hour
            Twitter.update("@#{from_user} ツイッターしてないで働け".append_timestamp)
          end
        end
      end

      @stream.userstream
    end

    private

    def crawl_twilog
      count = 0
      base_url = "http://twilog.org/nukokusa/month-"
      agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36"

      loop do
        month = (((count+12) % 12) +  1).to_s.rjust(2, '0')
        year  = (((count+12) / 12) + 12).to_s.rjust(2, '0')
        url = "#{base_url}#{year}#{month}"

        loop do
          puts url
          begin
            html = Nokogiri::HTML(open(url, 'User-Agent' => agent), nil, 'utf-8')
            ids    = html.css('.tl-tweet').map{|tag| tag.attributes['id'].to_s.sub('tw', '').to_i }
            tweets = html.css('.tl-text').map{|tag| tag.content }
          rescue
            puts "pass"
          end
          return if tweets.empty?

          ids.zip(tweets).each do |id, tweet|
            @markov.add_sentence(id, tweet)
            puts "#{id}: #{tweet} is added."
          end

          next_page = html.css('.nav-next > a')
          next_page.empty? ? break : url = next_page.attribute('href').value
          sleep 120
        end
        count += 1
      end
    end

    def twitter_auth
      config = @config['twitter']

      TweetStream.configure do |conf|
        conf.consumer_key       = config['consumer_key']
        conf.consumer_secret    = config['consumer_secret']
        conf.oauth_token        = config['access_token']
        conf.oauth_token_secret = config['access_token_secret']
        conf.auth_method       = :oauth
      end

      Twitter.configure do |conf|
        conf.consumer_key       = config['consumer_key']
        conf.consumer_secret    = config['consumer_secret']
        conf.oauth_token        = config['access_token']
        conf.oauth_token_secret = config['access_token_secret']
      end
    end
  end
end