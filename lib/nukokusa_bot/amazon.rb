#-*- coding: utf-8 -*-

module NukokusaBot
  class AmazonAssociate

    BASE_OPTION = {
      operation:      'ItemSearch',
      search_index:   'All',
      availability:   'Available',
      response_group: 'Large'
    }

    def initialize(conf)
      Amazon::Ecs.options = {
        :associate_tag     => conf['associate_tag'],
        :AWS_access_key_id => conf['access_key'],
        :AWS_secret_key    => conf['secret_key'],
        :country => 'jp'
      }
    end

    def search(keyword, opts = {})
      items = Amazon::Ecs.item_search(keyword, BASE_OPTION.merge(opts)).items

      items.collect do |item|
        {
          title: item.get("ItemAttributes/Title"),
          price: item.get("ItemAttributes/ListPrice/FormattedPrice"),
          url:   item.get("DetailPageURL")
        }
      end
    end
  end
end