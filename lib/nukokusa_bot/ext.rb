#-*- coding: utf-8 -*-

class String
  def contains_url?
    !!self.match(%r!https?://[\w~=/:%#&\$\?\(\)\.\+\-]+!)
  end

  def delete_mention
    self.gsub(/@[A-Za-z0-9_]+/, '')
  end

  def append_timestamp
    stamp = "[#{Time.now.strftime('%Y-%m-%d %H:%M:%S')}]"
    self.end_with?(' ') ? "#{self}#{stamp}" : "#{self} #{stamp}"
  end
end

class Hash
  def slice(*keys)
    self.select{ |key, val| keys.include? key }
  end
end