#-*- coding: utf-8 -*-

module NukokusaBot
  class MarkovChain

    def initialize
      @mecab = Natto::MeCab.new

      initialize_database
    end

    def add_sentence(id, text)
      return if registered_id?(id)
      return if text.contains_url?
      store_tweet(id, text)

      id_chain = parse_text(text.delete_mention).collect{ |word|
        find_or_create_word(word) rescue puts
      }.each_cons(2){ |pre, suc|
        create_or_update_link(pre, suc) rescue puts
      }
    end

    def generate_sentence(limit = 140)
      loop do
        chain = [fetch_random_word]
        loop do
          last_id = chain.last['id']
          next_word = fetch_next_word(last_id)
          break if rand < 0.1 || next_word['id'].nil?
          chain << next_word
        end
        sentence = chain.collect{|word| word['surface'] }.join
        return sentence if (1 .. limit).include? sentence.length
      end
    end

    def delete_words_and_links
      @db_conn.execute("DROP TABLE IF EXISTS words;")
      @db_conn.execute("DROP TABLE IF EXISTS links;")

      initialize_database
    end

    def rebuild_dictionary
      delete_words_and_links

      @db_conn.execute("SELECT body FROM tweets;").each do |tweet|
        text = tweet['body']
        parse_text(text.delete_mention).collect{ |word|
          find_or_create_word(word)
        }.each_cons(2){ |pre, suc|
          create_or_update_link(pre, suc)
        }
      end
    end

    private

    def parse_text(text)
      @mecab.parse(text).split("\n")[0 .. -2].collect{|morph| morph.split("\t").first }
    end

    def initialize_database
      db_dir  = File.join(File.dirname(__FILE__), '..', '..', 'db')
      ActiveRecord::Base.establish_connection(
        adapter:  'sqlite3',
        database: File.join(db_dir, (ENV['nukokusabot.env'] || 'dev')+'.db'),
        encoding: 'utf8'
      )

      @db_conn = ActiveRecord::Base.connection
      @db_conn.execute <<-SQL
        CREATE TABLE IF NOT EXISTS words
        (id         INTEGER PRIMARY KEY AUTOINCREMENT,
         surface    TEXT    NOT NULL UNIQUE);
      SQL
      @db_conn.execute <<-SQL
        CREATE TABLE IF NOT EXISTS links
        (pre_id INTEGER NOT NULL,
         suc_id INTEGER NOT NULL,
         count  INTEGER DEFAULT 1,
         UNIQUE (pre_id, suc_id) );
      SQL
      @db_conn.execute 'CREATE INDEX IF NOT EXISTS pre_id_index ON links(pre_id);'
      @db_conn.execute <<-SQL
        CREATE TABLE IF NOT EXISTS tweets
        (id   INTEGER PRIMARY KEY NOT NULL,
         body TEXT NOT NULL);
      SQL
    end

    def find_or_create_word(surface)
      return nil if surface =~ /;/
      id = @db_conn.execute("SELECT id FROM words WHERE surface = '#{surface}';").first
      if id.nil?
        @db_conn.execute("INSERT INTO words (surface) VALUES ('#{surface}');")
        id = @db_conn.execute("SELECT id FROM words WHERE surface = '#{surface}';").first
      end
      return id['id']
    end

    def create_or_update_link(pre_id, suc_id)
      return unless pre_id.is_a?(Integer) && suc_id.is_a?(Integer)
      count = @db_conn.execute("SELECT count FROM links WHERE pre_id = #{pre_id} AND suc_id = #{suc_id};").first
      if count.nil?
        @db_conn.execute("INSERT INTO links (pre_id, suc_id) VALUES (#{pre_id}, #{suc_id});")
      else
        @db_conn.execute("UPDATE links SET count = #{count['count']+1} WHERE pre_id = #{pre_id} AND suc_id = #{suc_id};")
      end
    end

    def store_tweet(id, body)
      return unless id.is_a?(Integer) && body.is_a?(String)
      @db_conn.execute("INSERT INTO tweets (id, body) VALUES (#{id}, '#{body}');") rescue return
    end

    def fetch_random_word
      @db_conn.execute("SELECT id, surface FROM words ORDER BY RANDOM() LIMIT 1;").first
    end

    def fetch_next_word(pre_id)
      return nil unless pre_id.is_a?(Integer)
      id = @db_conn.execute("SELECT id, surface FROM words WHERE id = (SELECT suc_id FROM links WHERE pre_id = #{pre_id} ORDER BY RANDOM() LIMIT 1);").first
      id.nil? ? {'id' => nil} : id
    end

    def registered_id?(id)
      return true unless id.is_a?(Integer)
      !(@db_conn.execute("SELECT * FROM tweets WHERE id = #{id};").empty?)
    end
  end
end